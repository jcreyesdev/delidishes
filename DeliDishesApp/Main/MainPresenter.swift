import UIKit
import Kingfisher


class MainPresenter: MainPresenterProtocol {
    
    weak var view: MainViewProtocol?
    var interactor: MainInteractorProtocol?
    var router: MainRouterProtocol?
    
    var timer: Timer = Timer()
    var meals: [Meal] = []
    
    
    // MARK: Call Service Meal Random
    @objc func getRequestCallMealRandom() {
        let url = "https://www.themealdb.com/api/json/v1/1/random.php"
        interactor?.getRequestCallMealRandomWith(url: url)
    }
    
    func getMealRandomSuccess(result: Meals) {
        if let urlImage = result.meals?[0].strMealThumb, let name = result.meals?[0].strMeal {
            let url = URL(string: urlImage.replacingOccurrences(of: "\\", with: ""))
            showNewImage(url: url!, name: name)
        }
    }
    
    func showNewImage(url: URL, name: String) {
        view?.bannerImage.kf.setImage(with: url, options: [.transition(.fade(0.5))])
        view?.mealNameView.isHidden = false
        view?.mealNameView.layer.cornerRadius = 12.0
        view?.mealNameLabel.text = name
    }
    
    func getMealRandomFailure() {
        timer.invalidate()
        countThirtySeconds()
    }
    
    func countThirtySeconds() {
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(getRequestCallMealRandom), userInfo: nil, repeats: true)
    }
    
    
    // MARK: Call Service Meals list
    func getRequestCallMeals(string: String) {
        if !string.isEmpty {
            let str = string.replacingOccurrences(of: " ", with: "_")
            let url = "https://www.themealdb.com/api/json/v1/1/search.php?s=\(str)"
            interactor?.getRequestCallMealsWith(url: url)
        } else {
            showMessageListIsEmpty(message: "Enter a dish to get its detail")
        }
    }
    
    func getMealsSuccess(result: Meals) {
        if let meals = result.meals {
            if !meals.isEmpty {
                self.meals = meals
                view?.mealsTableView.isHidden = false
                view?.messageView.isHidden = true
                view?.reloadTableView()
            }
        } else {
            showMessageListIsEmpty(message: "Enter a dish to get its detail")
        }
    }
    
    func getMealsFailure() {
        view?.mealsTableView.isHidden = true
        view?.messageView.isHidden = false
        view?.messageLabel.text = "There was an error getting the dishes, please try again"
    }
    
    func showMessageListIsEmpty(message: String) {
        meals.removeAll()
        view?.mealsTableView.isHidden = true
        view?.messageView.isHidden = false
        view?.messageLabel.text = message
        view?.reloadTableView()
    }
    
    
    // MARK: UITableViewDataSource & UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: view!.mealCell) as! MealCell
        let index = indexPath.row
        let data = meals[index]
        let urlImage = data.strMealThumb?.replacingOccurrences(of: "\\", with: "")
        
        cell.selectionStyle = .none
        cell.mealImage.layer.cornerRadius = 8.0
        cell.mealImage.kf.setImage(with: URL(string: urlImage!), options: [.transition(.fade(0.5))])
        cell.mealTitleLabel.text = data.strMeal ?? ""
        cell.mealCategoryLabel.text = data.strCategory ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let meal = meals[indexPath.row]
        router?.goToDetail(meal: meal)
    }
    
}
