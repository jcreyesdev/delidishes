import UIKit
import Kingfisher


class MainView: UIViewController, MainViewProtocol {
    
    var presenter: MainPresenterProtocol?
    
    // IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var mealNameView: UIView!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealsTableView: UITableView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var mealCell: String = "MealCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.getRequestCallMealRandom()
        setUpUI()
        setUpSearchBar()
        registerCell(tableView: mealsTableView)
        setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter?.countThirtySeconds()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        presenter?.timer.invalidate()
    }
    
    func setUpUI() {
        title = "Deli"
        mealsTableView.isHidden = true
        messageView.isHidden = false
        messageLabel.text = "Enter a dish to get its detail"
        mealNameView.isHidden = true
        bannerView.layer.cornerRadius = 8.0
        hideKeyboardWhenTappedAround()
    }
    
    func setUpSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = "Search meal"
    }
    
    func registerCell(tableView: UITableView) {
        let idetifier = mealCell
        let nib = UINib(nibName: idetifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: idetifier)
    }
    
    func setUpTableView() {
        mealsTableView.delegate = self
        mealsTableView.dataSource = self
        mealsTableView.estimatedRowHeight = 100.0
    }
    
    func reloadTableView() {
        mealsTableView.reloadData()
    }
    
}


// MARK: UISearchBarDelegate
extension MainView: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.getRequestCallMeals(string: searchText)
    }
    
}


// MARK: UITableViewDataSource & UITableViewDelegate
extension MainView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.numberOfSections(in: tableView) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.tableView(tableView, numberOfRowsInSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter?.tableView(tableView, cellForRowAt: indexPath) ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.tableView(tableView, didSelectRowAt: indexPath)
    }
    
}
