import UIKit
import AVFoundation
import SwiftUI


class MainInteractor: MainInteractorProtocol {
    
    weak var presenter: MainPresenterProtocol?
    
    let network = Network()
    
    
    func getRequestCallMealRandomWith(url: String) {
        network.getRequestCallMealRandomWith(url: url)
        network.completionHandleMealRandom { [weak self] meal, status, message in
            if status {
                guard let self = self else { return }
                guard let meal = meal else { return }
                
                self.presenter?.getMealRandomSuccess(result: meal)
            } else {
                self?.presenter?.getMealRandomFailure()
            }
        }
    }
    
    func getRequestCallMealsWith(url: String) {
        network.getRequestCallMealsWith(url: url)
        network.completionHandleMeals { [weak self] meal, status, message in
            if status {
                guard let self = self else { return }
                guard let meal = meal else { return }
                
                self.presenter?.getMealsSuccess(result: meal)
            } else {
                self?.presenter?.getMealsFailure()
            }
        }
    }
    
}
