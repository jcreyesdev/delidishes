import UIKit


class MainRouter: MainRouterProtocol {
    
    weak var view: UIViewController?
    
    
    func goToDetail(meal: Meal) {
        let mealDetail = DetailModule.build(meal: meal)
        self.view?.navigationController?.pushViewController(mealDetail, animated: true)
    }
    
}
