import UIKit
import Foundation


// MARK: - View
protocol MainViewProtocol: AnyObject {
    var presenter: MainPresenterProtocol? { get set }
    var mealCell: String { get }
    var bannerView: UIView! { get set }
    var bannerImage: UIImageView! { get set }
    var mealNameView: UIView! { get set }
    var mealNameLabel: UILabel! { get set }
    var mealsTableView: UITableView! { get set }
    var messageView: UIView! { get set }
    var messageLabel: UILabel! { get set }
    
    func reloadTableView()
}


// MARK: - Presenter
protocol MainPresenterProtocol: AnyObject {
    var timer: Timer { get }
    
    func countThirtySeconds()
    func getRequestCallMealRandom()
    func getMealRandomSuccess(result: Meals)
    func getMealRandomFailure()
    func getRequestCallMeals(string: String)
    func getMealsSuccess(result: Meals)
    func getMealsFailure()
    func numberOfSections(in tableView: UITableView) -> Int
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
}


// MARK: - Interactor
protocol MainInteractorProtocol: AnyObject {
    var presenter: MainPresenterProtocol? { get set }
    
    func getRequestCallMealRandomWith(url: String)
    func getRequestCallMealsWith(url: String)
}


// MARK: - Router
protocol MainRouterProtocol: AnyObject {
    func goToDetail(meal: Meal)
}
