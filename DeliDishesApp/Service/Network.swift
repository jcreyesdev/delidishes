import Foundation
import Alamofire


class Network {
    
    typealias mealRandom = (_ meals: Meals?, _ status: Bool, _ message: String) -> Void
    var mealRandomCallBack: mealRandom?
    
    typealias meals = (_ meals: Meals?, _ status: Bool, _ message: String) -> Void
    var mealsCallBack: meals?
    
    
    // MARK: GetMealRandom
    func getRequestCallMealRandomWith(url: String) {
        AF.request(url, method: .get, encoding: URLEncoding.default).response { (response) in
            guard let data = response.data else {
                self.mealRandomCallBack?(nil, false, "")
                debugPrint("👨🏻‍💻 NETWORK RESPONSE ERROR")
                return
            }

            do {
                let meals = try JSONDecoder().decode(Meals.self, from: data)
                
                self.mealRandomCallBack?(meals, true, "")
                
                guard let object = try? JSONSerialization.jsonObject(with: response.data!, options: []),
                      let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
                      let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return }
                print("👨🏻‍💻 NETWORK RESPONSE SUCCESS -> \(prettyPrintedString)")
            } catch {
                self.mealRandomCallBack?(nil, false, error.localizedDescription)
                print("👨🏻‍💻 NETWORK RESPONSE ERROR -> \(error.localizedDescription)")
            }
        }
    }
    
    func completionHandleMealRandom(callBack: @escaping mealRandom) {
        self.mealRandomCallBack = callBack
    }
    
    
    // MARK: GetMealsList
    func getRequestCallMealsWith(url: String) {
        AF.request(url, method: .get, encoding: URLEncoding.default).response { (response) in
            guard let data = response.data else {
                self.mealsCallBack?(nil, false, "")
                debugPrint("👨🏻‍💻 NETWORK RESPONSE ERROR")
                return
            }

            do {
                let meals = try JSONDecoder().decode(Meals.self, from: data)
                self.mealsCallBack?(meals, true, "")
                
                guard let object = try? JSONSerialization.jsonObject(with: response.data!, options: []),
                      let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
                      let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return }
                print("👨🏻‍💻 NETWORK RESPONSE SUCCESS -> \(prettyPrintedString)")
            } catch {
                self.mealsCallBack?(nil, false, error.localizedDescription)
                print("👨🏻‍💻 NETWORK RESPONSE ERROR -> \(error.localizedDescription)")
            }
        }
    }
    
    func completionHandleMeals(callBack: @escaping meals) {
        self.mealsCallBack = callBack
    }
    
}
