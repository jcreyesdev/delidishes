import UIKit

class IngredientCell: UICollectionViewCell {
    
    // IBOutlets
    @IBOutlet weak var ingredientView: UIView!
    @IBOutlet weak var ingredientLabel: UILabel!
    @IBOutlet weak var measureLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
