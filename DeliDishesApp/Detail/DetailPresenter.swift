import UIKit


class DetailPresenter: DetailPresenterProtocol {
    
    weak var view: DetailViewProtocol?
    var interactor: DetailInteractorProtocol?
    var router: DetailRouterProtocol?
    
    var ingredients: [Ingredient] = []
    
    
    func getAllIngredients() {
        var strIngredients: [(String, String)] = []
        var strMeasures: [(String, String)] = []
        var names: [String] = []
        var measures: [String] = []
        do {
            if let properties = try view?.meal.allProperties() {
                let _ = properties.map { (key, value) in
                    if let value = value as? String {
                        
                        if key.starts(with: "strIngredient") {
                            strIngredients.append((key, value))
                        }
                        
                        if key.starts(with: "strMeasure") {
                            strMeasures.append((key, value))
                        }
                    }
                }
            }
            
            strIngredients = strIngredients.sorted(by: <)
            for (_ , value) in strIngredients {
                if value != "" && value != " " {
                    names.append(value)
                }
            }
            
            strMeasures = strMeasures.sorted(by: <)
            for (_ , value) in strMeasures {
                if value != "" && value != " " {
                    measures.append(value)
                }
            }
            
            for i in 0 ... names.count - 1 {
                ingredients.append(Ingredient(ingredient: names[i], measure: measures[i]))
            }
        } catch {
            print("Error getting ingredients")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: view!.ingredientCell, for: indexPath) as! IngredientCell
        let data = ingredients[indexPath.row]

        cell.ingredientView.layer.cornerRadius = 8.0
        cell.ingredientLabel.text = data.ingredient
        cell.measureLabel.text = data.measure

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 148.0, height: 100.0)
    }
}
