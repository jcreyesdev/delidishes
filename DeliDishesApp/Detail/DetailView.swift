import UIKit
import Kingfisher


class DetailView: UIViewController, DetailViewProtocol {
    
    var presenter: DetailPresenterProtocol?
    
    // IBOutlets
    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var mealNameView: UIView!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var mealCategoryView: UIView!
    @IBOutlet weak var mealCategoryLabel: UILabel!
    @IBOutlet weak var ingredientTitleLabel: UILabel!
    @IBOutlet weak var ingredientCollectionView: UICollectionView!
    @IBOutlet weak var instructionsTitleLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    var meal: Meal
    var ingredientCell: String = "IngredientCell"
    
    
    init(meal: Meal) {
        self.meal = meal
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        setUpSearchBar()
        registerCell(collectionView: ingredientCollectionView)
        getAllIngredients()
    }
    
    func setUpUI() {
        title = meal.strMeal
        let urlImage = meal.strMealThumb?.replacingOccurrences(of: "\\", with: "")
        mealImage.kf.setImage(with: URL(string: urlImage!), options: [.transition(.fade(0.5))])
        mealNameView.layer.cornerRadius = 12.0
        mealNameLabel.text = meal.strMeal
        mealCategoryView.layer.cornerRadius = 12.0
        mealCategoryLabel.text = meal.strCategory
        ingredientTitleLabel.text = "Ingredients"
        instructionsTitleLabel.text = "Instructions"
        instructionsLabel.text = meal.strInstructions
    }
    
    func setUpSearchBar() {
        ingredientCollectionView.dataSource = self
        ingredientCollectionView.delegate = self
    }
    
    func registerCell(collectionView: UICollectionView) {
        let idetifier = ingredientCell
        let nib = UINib(nibName: idetifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: idetifier)
    }
    
    func getAllIngredients() {
        presenter?.getAllIngredients()
    }
    
}


// MARK: UICollectionViewDataSource, UICollectionViewDelegate & UICollectionViewDelegateFlowLayout
extension DetailView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter?.numberOfSections(in: collectionView) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.collectionView(collectionView, numberOfItemsInSection: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return presenter?.collectionView(collectionView, cellForItemAt: indexPath) ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return presenter?.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath) ?? CGSize()
    }
    
}
