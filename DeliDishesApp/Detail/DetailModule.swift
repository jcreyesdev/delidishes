import UIKit


class DetailModule {
    
    static func build(meal: Meal) -> UIViewController {
        let view = DetailView(meal: meal)
        let interactor = DetailInteractor()
        let presenter = DetailPresenter()
        let router = DetailRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.view = view
        
        return view
    }    
}
