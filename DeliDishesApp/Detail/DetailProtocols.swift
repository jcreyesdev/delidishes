import UIKit
import Foundation


// MARK: - View
protocol DetailViewProtocol: AnyObject {
    var presenter: DetailPresenterProtocol? { get set }
    var ingredientCell: String { get }
    var meal: Meal { get set }
}


// MARK: - Presenter
protocol DetailPresenterProtocol: AnyObject {
    func getAllIngredients()
    func numberOfSections(in collectionView: UICollectionView) -> Int
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
}


// MARK: - Interactor
protocol DetailInteractorProtocol: AnyObject {
    var presenter: DetailPresenterProtocol? { get set }
}


// MARK: - Router
protocol DetailRouterProtocol: AnyObject {
    
}
