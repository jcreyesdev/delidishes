import XCTest
@testable import DeliDishesApp


class TestDetailInteractor: XCTestCase {
    var sut: DetailInteractor!
    var mockPresenter: DetailPresenterMockInteractor!
    
    
    override func setUp() {
        super.setUp()
        
        sut = DetailInteractor()
        mockPresenter = DetailPresenterMockInteractor()
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockPresenter = nil
    }
    
    
}


// MARK: Mocks
class DetailPresenterMockInteractor: DetailPresenterProtocol {
    func getAllIngredients() { }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { return 0 }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return 0 }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell { return UICollectionViewCell() }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize { return CGSize() }
    
}
