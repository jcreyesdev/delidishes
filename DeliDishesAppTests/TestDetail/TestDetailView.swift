import XCTest
@testable import DeliDishesApp


class TestDetailView: XCTestCase {
    var sut: DetailView!
    var mockPresenter: DetailPresenterMockView!
    
    
    override func setUp() {
        super.setUp()
        
        sut = DetailView(meal: Meal())
        mockPresenter = DetailPresenterMockView()
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockPresenter = nil
    }
    
    func testTitleCheck() {
        let title = sut.title
        XCTAssertNotEqual(title, "")
    }
    
}


// MARK: Mocks
class DetailPresenterMockView: DetailPresenterProtocol {
    func getAllIngredients() { }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { return 0 }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return 0 }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell { return UICollectionViewCell() }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize { return CGSize() }
    
}
