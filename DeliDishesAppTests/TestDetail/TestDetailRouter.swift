import XCTest
@testable import DeliDishesApp


class TestDetailRouter: XCTestCase {
    var sut: DetailRouter!
    var mockView: DetailViewMockRouter!
    
    
    override func setUp() {
        super.setUp()
        
        sut = DetailRouter()
        mockView = DetailViewMockRouter()
        sut.view = mockView
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockView = nil
    }
    
    
}


// MARK: Mocks
class DetailViewMockRouter: UIViewController { }
