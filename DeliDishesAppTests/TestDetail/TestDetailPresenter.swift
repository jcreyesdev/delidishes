import XCTest
@testable import DeliDishesApp


class TestDetailPresenter: XCTestCase {
    var sut: DetailPresenter!
    var mockView: DetailViewMockPresenter!
    var mockInteractor: DetailInteractorMockPresenter!
    var mockRouter: DetailRouterMockPresenter!
    
    
    override func setUp() {
        super.setUp()
        
        sut = DetailPresenter()
        mockView = DetailViewMockPresenter()
        mockInteractor = DetailInteractorMockPresenter()
        mockRouter = DetailRouterMockPresenter()
        sut.view = mockView
        sut.interactor = mockInteractor
        sut.router = mockRouter
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockView = nil
        mockInteractor = nil
        mockRouter = nil
    }
    
}


// MARK: Mocks
class DetailViewMockPresenter: DetailViewProtocol {
    var presenter: DetailPresenterProtocol?
    var ingredientCell: String = ""
    var meal: Meal = Meal()
    
}

class DetailInteractorMockPresenter: DetailInteractorProtocol {
    var presenter: DetailPresenterProtocol?
    
}

class DetailRouterMockPresenter: DetailRouterProtocol {
    
}
