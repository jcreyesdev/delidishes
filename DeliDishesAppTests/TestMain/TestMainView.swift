import XCTest
@testable import DeliDishesApp


class TestMainView: XCTestCase {
    var sut: MainView!
    var mockPresenter: MainPresenterMockView!
    
    
    override func setUp() {
        super.setUp()
        
        sut = MainView()
        mockPresenter = MainPresenterMockView()
        sut.presenter = mockPresenter
        let _ = sut.view
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockPresenter = nil
    }
    
    
    func testTitleCheck() {
        let title = sut.title
        XCTAssertEqual(title, "Deli")
        XCTAssertNotEqual(title, "")
    }
    
    func testTableViewIsHiddenInitialCheck() {
        let isHidden = sut.mealsTableView.isHidden
        XCTAssertEqual(isHidden, true)
    }
    
    func testMessageViewIsShowInitialCheck() {
        let isHidden = sut.messageView?.isHidden
        XCTAssertEqual(isHidden, false)
    }
    
    func testMessageLabelCheck() {
        let message = sut.messageLabel.text
        XCTAssertEqual(message, "Enter a dish to get its detail")
    }
    
}


// MARK: Mocks
class MainPresenterMockView: MainPresenterProtocol {
    var timer: Timer = Timer()
    
    func countThirtySeconds() { }
    
    func getRequestCallMealRandom() { }
    
    func getMealRandomSuccess(result: Meals) { }
    
    func getMealRandomFailure() { }
    
    func getRequestCallMeals(string: String) { }
    
    func getMealsSuccess(result: Meals) { }
    
    func getMealsFailure() { }
    
    func numberOfSections(in tableView: UITableView) -> Int { return 0 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return 0 }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { return UITableViewCell() }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { }
    
}
