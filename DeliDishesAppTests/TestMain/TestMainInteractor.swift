import XCTest
@testable import DeliDishesApp


class TestMainInteractor: XCTestCase {
    var sut: MainInteractor!
    var mockPresenter: MainPresenterMockInteractor!
    
    
    override func setUp() {
        super.setUp()
        
        sut = MainInteractor()
        mockPresenter = MainPresenterMockInteractor()
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockPresenter = nil
    }
    
    
}


// MARK: Mocks
class MainPresenterMockInteractor: MainPresenterProtocol {
    var timer: Timer = Timer()
    
    func countThirtySeconds() { }
    
    func getRequestCallMealRandom() { }
    
    func getMealRandomSuccess(result: Meals) { }
    
    func getMealRandomFailure() { }
    
    func getRequestCallMeals(string: String) { }
    
    func getMealsSuccess(result: Meals) { }
    
    func getMealsFailure() { }
    
    func numberOfSections(in tableView: UITableView) -> Int { return 0 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return 0 }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { return UITableViewCell() }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { }
    
}
