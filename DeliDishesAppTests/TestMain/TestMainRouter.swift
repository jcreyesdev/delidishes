import XCTest
@testable import DeliDishesApp


class TestMainRouter: XCTestCase {
    var sut: MainRouter!
    var mockView: MainViewMockRouter!
    
    
    override func setUp() {
        super.setUp()
        
        sut = MainRouter()
        mockView = MainViewMockRouter()
        sut.view = mockView
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockView = nil
    }
    
    
}


// MARK: Mocks
class MainViewMockRouter: UIViewController { }
