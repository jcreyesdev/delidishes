import XCTest
@testable import DeliDishesApp


class TestMainPresenter: XCTestCase {
    var sut: MainPresenter!
    var mockView: MainViewMockPresenter!
    var mockInteractor: MainInteractorMockPresenter!
    var mockRouter: MainRouterMockPresenter!
    
    
    override func setUp() {
        super.setUp()
        
        sut = MainPresenter()
        mockView = MainViewMockPresenter()
        mockInteractor = MainInteractorMockPresenter()
        mockRouter = MainRouterMockPresenter()
        sut.view = mockView
        sut.interactor = mockInteractor
        sut.router = mockRouter
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        mockView = nil
        mockInteractor = nil
        mockRouter = nil
    }
    
}


// MARK: Mocks
class MainViewMockPresenter: MainViewProtocol {
    var presenter: MainPresenterProtocol?
    var mealCell: String = ""
    var bannerView: UIView!
    var bannerImage: UIImageView!
    var mealNameView: UIView!
    var mealNameLabel: UILabel!
    var mealsTableView: UITableView!
    var messageView: UIView!
    var messageLabel: UILabel!
    
    func reloadTableView() { }
    
}

class MainInteractorMockPresenter: MainInteractorProtocol {
    var presenter: MainPresenterProtocol?
    
    func getRequestCallMealRandomWith(url: String) { }
    
    func getRequestCallMealsWith(url: String) { }
    
}

class MainRouterMockPresenter: MainRouterProtocol {
    func goToDetail(meal: Meal) { }
    
}
